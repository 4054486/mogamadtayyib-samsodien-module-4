import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

String username = 'A';
String password = 'blank';
void main() {
  runApp(MaterialApp(
      home: AnimatedSplashScreen(
          duration: 1500,
          splash: Icons.home,
          nextScreen: Home(),
          splashTransition: SplashTransition.fadeTransition,
          backgroundColor: Colors.lightGreen),
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.lightGreen,
        ).copyWith(
          secondary: Colors.green,
        ),
        textTheme: const TextTheme(bodyText2: TextStyle(color: Colors.black)),
      ),
      routes: {
        'home': (context) => Home(),
        'register': (context) => Register(),
        'dashboard': (context) => Dashboard(),
        'Stuff': (context) => Feature1(),
        'Stuff2': (context) => Feature2(),
        'EditProfile': (context) => UserProfile(),
      }));
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login page'),
        centerTitle: true,
      ),
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
            Container(
                padding: EdgeInsets.all(10.0),
                child: Text('Please enter your login details:')),
            Container(
                width: 300.0,
                padding: EdgeInsets.all(10.0),
                child: TextField(
                    decoration: InputDecoration(hintText: 'Username'))),
            Container(
                width: 300.0,
                padding: EdgeInsets.all(10.0),
                child: TextField(
                    decoration: InputDecoration(hintText: 'Password'))),
            Container(
                padding: EdgeInsets.all(10.0),
                child: TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'dashboard');
                    },
                    child: const Text('Sign in'))),
            Container(
                padding: EdgeInsets.all(10.0),
                child: TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'register');
                    },
                    child: const Text('Register'))),
          ])),
    );
  }
}

class Register extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Register"),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: Text('Please create a username and password:')),
              Container(
                  width: 300.0,
                  padding: EdgeInsets.all(10.0),
                  child: TextField(
                      decoration: InputDecoration(hintText: 'Username'))),
              Container(
                  width: 300.0,
                  padding: EdgeInsets.all(10.0),
                  child: TextField(
                      decoration: InputDecoration(hintText: 'Password'))),
              Container(
                  width: 300.0,
                  padding: EdgeInsets.all(10.0),
                  child: TextField(
                      decoration:
                          InputDecoration(hintText: 'Confirm Password'))),
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, 'home');
                      },
                      child: Text('Create account'))),
            ]),
      ),
    );
  }
}

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
            Container(
                padding: EdgeInsets.all(10.0),
                child: Text('Welcome, this is your dashboard.')),
            Container(
                padding: EdgeInsets.all(10.0),
                child: Text(
                    'You can edit your profile by clicking the button in the bottom right.')),
            Container(
                padding: EdgeInsets.all(10.0),
                child: Text(
                    'Please click one of the button below to view the feature pages:')),
            Container(
                padding: EdgeInsets.all(10.0),
                child: TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'Stuff');
                    },
                    child: Text('Click if you are a cat person'))),
            Container(
                padding: EdgeInsets.all(10.0),
                child: TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'Stuff2');
                    },
                    child: Text('Click if you are a dog person'))),
          ])),
      floatingActionButton: FloatingActionButton(
        child: Text('Edit'),
        onPressed: () {
          Navigator.pushNamed(context, 'EditProfile');
        },
      ),
    );
  }
}

class UserProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit profile"),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: Text('Profile details:')),
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: Text('Username: ' + username)),
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: Text('Password: ' + password)),
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, 'dashboard');
                      },
                      child: Text('Confirm details'))),
            ]),
      ),
    );
  }
}

class Feature1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cat"),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: Image(
                    image: AssetImage('assets/cat.jpg'),
                    width: 500,
                    height: 500,
                  )),
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: Text(
                      'This is Peanut, Peanut is a cat, Peanut likes lazing about. Here is a rare case of Peanut not doing so.')),
            ]),
      ),
    );
  }
}

class Feature2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dog"),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: Image(
                    image: AssetImage('assets/dog.jpg'),
                    width: 500,
                    height: 500,
                  )),
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: Text(
                      'This is Rolo. Rolos loves the outdoors, Rolo also likes smelling everything. Rolo doesn\'t bite so go ahead and pet him.')),
            ]),
      ),
    );
  }
}
